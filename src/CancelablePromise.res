@@uncurried

type t<'a>
type error = Js.Promise2.error

@module("cancelable-promise") @new
external make: (
  (~resolve: 'a => unit, ~reject: error => unit, ~onCancel: (unit => unit) => unit) => unit
) => t<'a> = "CancelablePromise"

@module("cancelable-promise") @new @scope("CancelablePromise")
external resolve: 'a => t<'a> = "resolve"

@module("cancelable-promise") @new @scope("CancelablePromise")
external reject: error => t<'a> = "reject"

@module("cancelable-promise") @scope("CancelablePromise")
external isCancelable: 'a => bool = "isCancelable"

@send external then: (t<'a>, 'a => t<'b>) => t<'b> = "then"
@send external catch: (t<'a>, error => t<'b>) => t<'b> = "catch"
@send external finally: (t<'a>, unit => unit) => t<'a> = "finally"
@send external cancel: t<'a> => unit = "cancel"
@get external isCanceled: t<'a> => bool = "isCanceled"

@module("cancelable-promise")
external cancelable: promise<'a> => t<'a> = "cancelable"

external promise: t<'a> => promise<'a> = "%identity"

@module("cancelable-promise") @scope("CancelablePromise")
external all: array<t<'a>> => t<array<'a>> = "all"

@module("cancelable-promise") @scope("CancelablePromise")
external all2: ((t<'a>, t<'b>)) => t<('a, 'b)> = "all"

@module("cancelable-promise") @scope("CancelablePromise")
external all3: ((t<'a>, t<'b>, t<'c>)) => t<('a, 'b, 'c)> = "all"

@module("cancelable-promise") @scope("CancelablePromise")
external all4: ((t<'a>, t<'b>, t<'c>, t<'d>)) => t<('a, 'b, 'c, 'd)> = "all"

@module("cancelable-promise") @scope("CancelablePromise")
external all5: ((t<'a>, t<'b>, t<'c>, t<'d>, t<'e>)) => t<('a, 'b, 'c, 'd, 'e)> = "all"

@module("cancelable-promise") @scope("CancelablePromise")
external race: array<t<'a>> => t<'a> = "race"
