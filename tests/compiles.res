CancelablePromise.make((~resolve, ~reject, ~onCancel) => {
  resolve(. 1)
  reject->ignore
  onCancel(._ => Js.Console.log("Cancelled"))
})->ignore
