PATH := ./node_modules/.bin/:$(PATH)

install yarn.lock: package.json
	yarn install

RESCRIPT_FILES := $(shell find src/ -type f -name '*.res')

clean:
	rm -rf ./node_modules

compile: $(RESCRIPT_FILES) yarn.lock
	@if [ -n "$(INSIDE_EMACS)" ]; then \
	    NINJA_ANSI_FORCED=0 rescript build -with-deps; \
	else \
		rescript build -with-deps; \
	fi

format: $(RESCRIPT_FILES) install
	rescript format -all

publish: compile
	yarn publish --access public

.PHONY: clean install format publish
